describe('Users factory', function () {
    var Users;
    var userList = [
        {
            id: '1',
            name: 'Jane',
            role: 'Designer',
            location: 'New York',
            twitter: 'gijane'
        },
        {
            id: '2',
            name: 'Bob',
            role: 'Developer',
            location: 'New York',
            twitter: 'billybob'
        },
        {
            id: '3',
            name: 'Jim',
            role: 'Developer',
            location: 'Chicago',
            twitter: 'jimbo'
        },
        {
            id: '4',
            name: 'Bill',
            role: 'Designer',
            location: 'LA',
            twitter: 'dabill'
        },
        {
        id: '5',
        name: 'Ricardo',
        role: 'Frot-end',
        location: 'SP',
        twitter: '@ricdecastro'
      }
    ];

    var singleUser = {
        id: '2',
        name: 'Bob',
        role: 'Developer',
        location: 'New York',
        twitter: 'billybob'
    };

    //Antes de cada teste carregar nosso módulo api.users
    beforeEach(angular.mock.module('api.users'));

    //Antes de cada teste, defina nossa fábrica de Usuários injetados (_Users_) para nossa variável Usuários locais
    beforeEach(inject(function(_Users_){
        Users = _Users_;
    }));
    
    //Um teste simples para verificar se a fábrica do usuário existe
    it('fábrica existente', function(){
        expect(Users).toBeDefined();
    })

    //Um conjunto de testes para o nosso método Users.all ()
    describe('.all()', function(){
        //Um teste simples para verificar se o método existe
        it('O método existe', function(){
            expect(Users.all).toBeDefined();
        })

        //Um teste para verificar se chamar all () retorna a matriz de usuários que codificamos acima
        it('deve retornar uma lista codificada de usuários', function() {
            expect(Users.all()).toEqual(userList);
        });
    });

    //um conjunto de testes para o método findById
    describe('.findById()', function() {
        
        it('Verificando se o método findById existe', function() {
            expect(Users.findById).toBeDefined();
        }) 

        it('deve retornar um unico usuário com id 2', function() {
            expect(Users.findById('2')).toEqual(singleUser)
        })

        //Um teste para verificar que chamar findById () com um id que não existe, neste caso 'ABC', retorna indefinido
        it('deve retornar indefinido se o usuário não puder ser encontrado', function() {
            expect(Users.findById('ABC')).not.toBeDefined();
        });
    })
});
