(function() {
  'use strict';

  angular.module('api.users', [])
  .factory('Users', function() {
    var Users = {};

    var userList = [
      {
        id: '1',
        name: 'Jane',
        role: 'Designer',
        location: 'New York',
        twitter: 'gijane'
      },
      {
        id: '2',
        name: 'Bob',
        role: 'Developer',
        location: 'New York',
        twitter: 'billybob'
      },
      {
        id: '3',
        name: 'Jim',
        role: 'Developer',
        location: 'Chicago',
        twitter: 'jimbo'
      },
      {
        id: '4',
        name: 'Bill',
        role: 'Designer',
        location: 'LA',
        twitter: 'dabill'
      },
       {
        id: '5',
        name: 'Ricardo',
        role: 'Frot-end',
        location: 'SP',
        twitter: '@ricdecastro'
      }
      
    ];
    var singleUser = {
        id: '2',
        name: 'Bob',
        role: 'Developer',
        location: 'New York',
        twitter: 'billybob'
    }

    

    //Definindo todos para fazer nosso teste passar. Não precisa fazer nada ainda.

    Users.all = function() {
       // Returning the array of users. Eventually this will be an API call.
      return userList;
    };

    Users.findById = function(id) {
      return userList.find(function(user) {
        return user.id === id;
      });
    };


   return Users;
  });
})();
